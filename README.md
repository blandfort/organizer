# The MuscularBrain Organizer

The MuscularBrain (MB) is an organizer for storing and managing personal notes.
For doing so, all notes are stored as encrypted JSON files.

__Please note__: Even though notes are encrypted (using AES 256),
please don't blindly trust the security of this software.
So far, it has not specifically been tested for security.


## Basic Functionality

The main purpose of this organizer is to manage personal notes.

It uses the Note class for representing individual notes, which can be converted to JSON format.
To keep things simple, almost all information of a Note (other than metadata such as note type, creation date and modification date) are stored in a field "content".

You can link to other notes from within the contents of notes by using wiki-style links (see below).
The class MB is used to manage collections of Notes and includes further functionality such as searching notes or saving them to disk.
Contents of the organizer are encrypted when saving them to disk.

You can run the organizer with a GUI (based on tkinter) or directly use its Python classes.


## Installation

First of all:

- Clone the repository
- Create a local settings file: `cp .env.exmpl .env`
- Adjust the settings in `.env`

Then install the package:

- Optional: Create and activate a virtual environment
    - `python3 -m venv venv`
    - `source venv/bin/activate`
- Install the muscularbrain package: `pip install -e .`

Then you can run the code as explained below.


## Usage

### GUI

The default usage is to run the organizer with a GUI, which you can do by calling `python run.py`.

Note: On MacOS, this seems to require Python>=3.12 (with 3.11 it runs but there is a bug which causes a lot of inconvenience).


### Python version: Load MB as python package

You can access MB with Python directly.
This way you can easily analyze or manipulate your notes.

Basic code for loading in Python:

```
    from muscularbrain import MB
    # Adjust these paths to your needs
    # (Files are created newly if they don't already exist.)
    path = 'path/to/MB'  # Where the notes will be saved
    key_path = 'path/to/KEY'  # To store secret key used for encryption
    mb = MB(path, key_path)

    # Now mb.notes contains all your Notes ;)
```


### Patterns

When editing or creating Notes, the textual content is parsed by using the following syntax:

- Referring to other Notes (displayed as clickable links in the GUI): `[[<note_title>]]`
- Creating Notes: You can create new Notes from within an existing Note by writing the pattern `<note_type>{{<note_content>}}` (e.g. `todo{{Clarify this}}`), which will be replaced by a link when you save the content
- Tags: `#<tag>`
- Keyword tags: `#<kwtag>:<value>`

Note: These patterns are defined in [system.py](muscularbrain/system.py) and can be modified.


### Workspaces

To organize your working contexts, you can use workspaces.

Workspaces are notes of note type `workspace`. You can create them just like other notes but setting the note type accordingly.
They are then interpreted in a special way.

At any time, one workspace is active and you can switch to other workspaces.
For each workspace, open tabs are remembered in the UI, and you can bookmark individual notes.
So, switching to another workspace changes the open tabs and the bookmarks.

Workspaces don't affect access to other notes and notes don't belong to workspaces in a strict sense,
so from any workspace you can link to any note and access any note.


### Synchronization

MB runs locally and doesn't have built-in synchronization.

However, you can set the path to the organizer directory (`MB_PATH` in `.env`) to point to a directory within a synchronized folder. For example, you can point to an iCloud directory (In that case, be sure to set "Keep Downloaded" for that directory to avoid time delays).

Note that the key in this case should not reside in the same synchronized folder but either be stored on your local device only, or be in a folder that is synchronized using another provider.
