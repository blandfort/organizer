import os
import logging
import base64
from Crypto.Cipher import AES
from Crypto import Random

# Basic code from http://stackoverflow.com/questions/12524994/encrypt-decrypt-using-pycrypto-aes-256

log = logging.getLogger(__name__)


class AESCipher:
    def __init__( self, key=None, key_path=None ):
        '''Use key that is 32 bytes long for AES 256.'''
        assert key is not None or key_path is not None,"Need to specify 'key' or 'key_path'!"

        if key is None:
            if os.path.isfile(key_path):
                log.info("Reading secret key from file '%s' ..." % key_path)
                self.key = key_from_file(key_path)
            else:
                log.warning("No key found for encryption! Generating new key ...")
                self.key = generate_key_to_file(key_path)
        else:
            self.key = key
        #self.key = hashlib.sha256(key.encode()).digest()

    def encrypt( self, raw ):
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CFB, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt( self, enc ):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CFB, iv )
        return cipher.decrypt( enc[AES.block_size:] )

    def encrypt_file(self, filename, encr_filename=None, binary=True):
        '''Encrypt a file (that is not too big).'''
        #ciph = AESCipher(key_from_file())

        if not encr_filename:
            encr_filename = filename+'.encr'

        with open(filename, 'rb' if binary else 'r') as f:
            with open(encr_filename, 'wb') as w:
                w.write(self.encrypt(f.read()))

    def decrypt_file(self, filename, decr_filename=None, binary=True):
        #ciph = AESCipher(key_from_file())

        if not decr_filename:
            if filename[::-1].split('.', 1)[0][::-1] == 'encr':
                # if filename ends with .encr we just remove that
                decr_filename = filename[::-1].split('.', 1)[1][::-1]
            else:
                decr_filename = filename+'.decr'

        with open(filename, 'rb') as f:
            with open(decr_filename, 'wb' if binary else 'w') as w:
                w.write(self.decrypt(f.read()))


def generate_key_to_file(filename, block_size=32):
    # block size 32 for AES 256
    SECRET_KEY = Random.new().read(block_size)

    log.info("Writing secret key to file '%s' ..." % filename)
    with open(filename, 'wb') as f:
        f.write(SECRET_KEY)
    return SECRET_KEY

def key_from_file(filename):
    with open(filename, 'rb') as f:
        return f.read()
