# WARNING: These functions aren't tested much and aren't updated anymore.
# They are included so you have a basis for scripting migration code,
# but use them at your own risk!

import json
import os
import re

from .aes_crypto import AESCipher
from .system import (
    Note,
    MB,
    title_from_content,
    MAX_TITLE_LENGTH,
    REFERENCE_REGEX,
    REFERENCE_STRING,
    ENCODING
)


def split_json_file(path, key_path, new_path):
    """Take an encrypted JSON file (path) and create a directory with individual notes
    and other files (new_path)."""
    ciph = AESCipher(key_path=key_path)
    with open(path, 'rb') as f:
        encrypted_status = f.read()
    json_status = json.loads(ciph.decrypt(encrypted_status))

    # Write note files
    for i, note in enumerate(json_status["notes"]):
        # Note that notes here are in JSON format
        encrypted_note = ciph.encrypt(json.dumps(note).encode(ENCODING))
        if os.path.isfile(os.path.join(new_path, f"{i}.encr")):
            print("Note files already exists! Skipping note migration!")
            break
        with open(os.path.join(new_path, f"{i}.encr"), 'wb') as f:
            f.write(encrypted_note)

    # Write history
    if "history" in json_status:
        for log in json_status["history"]:
            encrypted_log = ciph.encrypt(json.dumps(log).encode(ENCODING))
            with open(os.path.join(new_path, "history.encr"), 'ab') as f:
                f.write(encrypted_json + "\n".encode(ENDOGING))

    # Write metadata
    metadata = {}
    if "active_workspace" in json_status:
        metadata["active_workspace"] = json_status["active_workspace"]
    if "enable_log" in json_status:
        metadata["enable_log"] = json_status["enable_log"]
    encrypted_metadata = ciph.encrypt(json.dumps(metadata).encode(ENCODING))
    with open(os.path.join(new_path, "metadata.encr"), 'wb') as f:
        f.write(encrypted_metadata)


def create_new_reference(res, title_mapping):
    note_title = res.group(1)

    if note_title in title_mapping:
        return REFERENCE_STRING % title_mapping[note_title]
    else:
        return REFERENCE_STRING % "DELETED NOTE"

def update_links_in_content(content: str, old_reference_regex, title_mapping):
    """Change references from the old to the new format in the given text."""
    new_content = re.sub(old_reference_regex,
        (lambda res: create_new_reference(res, title_mapping)), content)
    return new_content

def _modify_string(s: str, unique_strings: set) -> str:
    # Regular expression to check if string ends with " (vNUM)"
    pattern = r" \(v(\d+)\)$"

    # Check if the string already ends with " (vNUM)"
    match = re.search(pattern, s)

    if match:
        # Extract the current version number
        num = int(match.group(1))
        base_string = s[:match.start()]  # String without the version part
    else:
        # If no version, start with the base string as is
        num = 1
        base_string = s

    # Increment the version number until the string is unique
    new_string = f"{base_string} (v{num + 1})"
    while new_string in unique_strings:
        num += 1
        new_string = f"{base_string} (v{num})"

    return new_string

def _update_titles(status, old_title_from_content, old_reference_string):
    """Update titles to ensure that they are unique.

    Changes are done to 'status' directly.

    'old_title_from_content' should be a function taking a note content as str and
    returning the title as string according to the old format.
    'old_reference_string' should be a string with a placeholder, where the
    old title can be inserted to get the reference to the note according to the old format.

    Return a list of notes with long titles which can't be updated automatically."""
    titles = []
    #reference_mapping = {}  # Old reference: new reference
    title_mapping = {}  # Old title: new title (for notes where that changed)
    long_ones = []
    for note in status['notes']:
        tentative_title = title_from_content(note['content'])
        old_title = old_title_from_content(note['content'])
        if tentative_title not in titles:
            titles.append(tentative_title)
            title_mapping[old_title] = tentative_title
            #reference_mapping[old_reference_string % old_title] = tentative_title
        else:
            print(f"Found multiple versions of '{tentative_title}', attempting to rename")
            title = tentative_title[:]

            # Check if we can easily modify the title
            parts = note['content'].split('\n', 1)
            if len(parts[0]) > MAX_TITLE_LENGTH - 8:
                # These notes have to be manually fixed! (And hopefully are quire rare)
                long_ones.append(note)
            else:
                new_first_line = _modify_string(parts[0], set(titles))
                if len(parts)>1:
                    note['content'] = new_first_line + "\n" + parts[1]
                else:
                    note['content'] = new_first_line
                new_title = title_from_content(note['content'])

                titles.append(new_title)
                title_mapping[old_title] = new_title
                print(old_title, new_title)  # DEBUGGING
                #reference_mapping[old_reference_string % old_title] = REFERENCE_STRING % new_title

    return title_mapping, long_ones


def update_links(old_title_from_content, old_reference_string, old_reference_regex, old_path, key_path=None, new_path=None):
    """If regex patterns for links have changed, use this function to update a previous status.

    Note that information in MB.history is not modified!"""
    if new_path is None:
        if old_path.lower().endswith('.encr') or old_path.lower().endswith('.json'):
            new_path = old_path.rsplit('.', 1)[0] + '_migrated' + old_path.rsplit('.', 1)[1]
        else:  # No file ending
            new_path = old_path + '_migrated'

    for fpath in [new_path, new_path+'.encr', new_path+'.json']:
        assert not os.path.exists(fpath), "Output file already exists! Stopping to avoid data loss."

    # Read the previous version
    # (Note that we cannot use the current MB class for this, as regex have changed!)
    if key_path is not None:
        encr_filename = old_path + '.encr'
        with open(encr_filename, 'rb') as f:
            encrypted_json = f.read()
        ciph = AESCipher(key_path=key_path)
        json_status = ciph.decrypt(encrypted_json)
        status = json.loads(json_status)
    else:
        status = json.load(open(old_path, "r"))

    # Step 1: Make sure that note titles with new title regex are unique
    # (Otherwise we can't link to them properly)
    title_mapping, long_notes = _update_titles(status, old_title_from_content, old_reference_string)
    if len(long_notes)>0:
        # If note titles are too long, you have to manually update these notes and then try again
        print(f"Found {len(long_notes)} notes with long titles which can't be updated automatically. Returning these notes and stopping.")
        return {"Status": "Failed (long notes)", "Info": long_notes}
    print(f"Title updated for {len([1 for o, n in title_mapping.items() if o!=n])} notes.")

    # Verify that all titles are unique
    unique_titles = set([title_from_content(note['content']) for note in status['notes']])
    assert len(status['notes'])==len(unique_titles), f"{len(status['notes'])} notes but only {len(unique_titles)} unique titles!"

    # Step 2: Update all references accordingly
    print("Updating links in note contents ...")
    for note in status['notes']:
        note['content'] = update_links_in_content(note['content'], old_reference_regex, title_mapping)

    # Initialize the updated organizer
    mb = MB(path=new_path, key_path=key_path)
    print("Adding notes to updated organizer ...")
    mb.from_json(status=status)
    print(f"{len(mb.notes)} notes migrated.")

    # Now also update bookmarks and tabs
    print("Updating bookmarks and tabs ...")
    for workspace_title in mb.get_workspaces():
        workspace = mb[workspace_title]

        for old_title, new_title in title_mapping.items():
            if old_title==new_title:
                continue

            # Note that the metadata keys might not be set yet,
            # so we need to call self.get_tabs and self.get_bookmarks here
            workspace.metadata["tabs"] = [new_title if note_title==old_title else note_title
                for note_title in mb.get_tabs(workspace=workspace)]
            workspace.metadata["bookmarks"] = [new_title if note_title==old_title else note_title
                for note_title in mb.get_bookmarks(workspace=workspace)]

    print("Saving updated organizer status ...")
    mb.save()
    print("Done.")
    return {"Status": "Success", "Info": title_mapping}


def updating_links_example(old_path="files/MB_migrate", new_path="files/MB_migrated", key_path="files/KEY"):
    OLD_ALLOWED_CHARS = r'!:\.,?a-zA-Z0-9äöüÄÖÜß_\-\(\)\'" '

    def old_title_from_content(text):

        first_line = text.split('\n')[0].strip()

        filename = re.sub(rf'[^{OLD_ALLOWED_CHARS}]', "_", first_line)
        title = filename.strip()[:100].strip()
        return title

    OLD_REFERENCE_REGEX = rf'\[\[([{OLD_ALLOWED_CHARS}]+)\]\]'


    return update_links(old_path=old_path, key_path=key_path, new_path=new_path,
                 old_title_from_content=old_title_from_content, old_reference_string="[[%s]]",
                 old_reference_regex=OLD_REFERENCE_REGEX)
