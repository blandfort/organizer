from enum import Enum
from typing import Optional


class LinkType(Enum):
    CONTENT = "content"
    METADATA = "metadata"


class Link:
    def __init__(self, in_note: str, out_note: str, link_type: Optional[LinkType]=None):
        self.A = in_note
        self.B = out_note
        if link_type is None:
            self.type_ = LinkType.CONTENT
        else:
            self.type_ = link_type

    def __repr__(self):
        return f'"{self.A}".{self.type_.value} -> "{self.B}"'

    def __eq__(self, link2):
        if self.A==link2.A and self.B==link2.B:
            return True
        return False

    def __hash__(self):
        return hash(repr(self))
