import re
import os
import logging
import json
import tkinter as tk
from functools import partial
from tkinter import ttk
from tkinter import simpledialog
from tkinter.messagebox import askyesno, showwarning, showerror
from typing import Optional, Iterable

log = logging.getLogger(__name__)

from .system import (
    MB,
    title_from_content,
    create_note,
    Note,
    Workspace,
    REFERENCE_STRING,
    REFERENCE_REGEX,
)


class CustomNotebook(ttk.Notebook):
    """A ttk Notebook with close buttons on each tab

    This class is based on https://stackoverflow.com/a/39459376"""

    __initialized = False

    def __init__(self, closing_callback, *args, **kwargs):
        if not self.__initialized:
            self.__initialize_custom_style()
            CustomNotebook.__inititialized = True

        kwargs["style"] = "CustomNotebook"
        ttk.Notebook.__init__(self, *args, **kwargs)

        self._active = None

        self.bind("<ButtonPress-1>", self.on_close_press, True)
        self.bind("<ButtonRelease-1>", self.on_close_release)
        self.closing_callback = closing_callback

    def on_close_press(self, event):
        """Called when the button is pressed over the close button"""

        element = self.identify(event.x, event.y)

        if "close" in element:
            index = self.index("@%d,%d" % (event.x, event.y))
            self.state(['pressed'])
            self._active = index
            return "break"

    def on_close_release(self, event):
        """Called when the button is released"""
        if not self.instate(['pressed']):
            return

        element =  self.identify(event.x, event.y)
        if "close" not in element:
            # user moved the mouse off of the close button
            return

        index = self.index("@%d,%d" % (event.x, event.y))

        if self._active == index:
            # This part has been modified
            #self.forget(index)
            self.closing_callback(tab_index=self._active)
            self.event_generate("<<NotebookTabClosed>>")

        self.state(["!pressed"])
        self._active = None

    def __initialize_custom_style(self):
        style = ttk.Style()
        self.images = (
            tk.PhotoImage("img_close", data='''
                R0lGODlhCAAIAMIBAAAAADs7O4+Pj9nZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
                '''),
            tk.PhotoImage("img_closeactive", data='''
                R0lGODlhCAAIAMIEAAAAAP/SAP/bNNnZ2cbGxsbGxsbGxsbGxiH5BAEKAAQALAAA
                AAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU5kEJADs=
                '''),
            tk.PhotoImage("img_closepressed", data='''
                R0lGODlhCAAIAMIEAAAAAOUqKv9mZtnZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
            ''')
        )

        style.element_create("close", "image", "img_close",
                            ("active", "pressed", "!disabled", "img_closepressed"),
                            ("active", "!disabled", "img_closeactive"), border=8, sticky='')
        style.layout("CustomNotebook", [("CustomNotebook.client", {"sticky": "nswe"})])
        style.layout("CustomNotebook.Tab", [
            ("CustomNotebook.tab", {
                "sticky": "nswe",
                "children": [
                    ("CustomNotebook.padding", {
                        "side": "top",
                        "sticky": "nswe",
                        "children": [
                            ("CustomNotebook.focus", {
                                "side": "top",
                                "sticky": "nswe",
                                "children": [
                                    ("CustomNotebook.label", {"side": "left", "sticky": ''}),
                                    ("CustomNotebook.close", {"side": "left", "sticky": ''}),
                                ]
                            })
                        ]
                    })
                ]
            })
        ])
        style.configure("CustomNotebook.Tab", padding=2)


class MBUI:

    applicationName = 'MuscularBrain'

    def __init__(
        self,
        mb: MB,
        width: int=600,
        height: int=700,
        open_links_in_new_frames: bool=True,
    ):
        self.mb = mb
        self.links_in_new_frames = open_links_in_new_frames

        self.window = tk.Tk()
        self.window.geometry(f'{width}x{height}')
        self.width = width
        self.height = height
        self.tabs = []
        self.display()
        self.window.mainloop()

    def _set_title(self):
        self.window.title(self.applicationName + ": " + self.mb.active_workspace)

    def do(self, action, **kwargs):
        # Get index of currently active tab
        tab_index = self.tab_parent.index(self.tab_parent.select())
        current_tab = self.tabs[tab_index]

        # In case the user tries to open an already open note, switch to that
        # tab instead
        if action=='open' and 'note_title' in kwargs:
            for i, tab in enumerate(self.tabs):
                if type(tab) is NoteTab and tab.currentNote.get()==kwargs['note_title']:
                    self.tab_parent.select(i)
                    return

        # Pass the action to the active tab
        current_tab.do(action=action, **kwargs)

        # Just in case, we always update the menu
        self.update_menu()

    def update_menu(self):
        # First reset the menus
        self.recentMenu.delete(0, 'end')
        self.workspaceMenu.delete(0, 'end')
        self.bookmarkMenu.delete(0, 'end')

        recent_notes = self.mb.search(order_by='lastchange', not_note_types=Workspace.name, limit=10)
        for note in recent_notes:
            title = note['title']
            self.recentMenu.add_command(label=title, command=partial(self.do, action='open', note_title=title))

        for workspace in self.mb.get_workspaces():
            if workspace!=self.mb.active_workspace:
                self.workspaceMenu.add_command(label=workspace, command=partial(self.switch_to_workspace, workspace))
            else:
                # If the workspace is already open, we open the corresponding Note if selected
                self.workspaceMenu.add_command(label=workspace, command=partial(self.do, action='open', note_title=workspace))

        self.bookmarkMenu.add_command(label="[Un-/Bookmark This]", command=self.bookmark_current)
        for note_title in self.mb.get_bookmarks():
            self.bookmarkMenu.add_command(label=note_title, command=partial(self.do, action='open', note_title=note_title))

    def bookmark_current(self):
        """Bookmark the current Note"""
        tab_index = self.tab_parent.index(self.tab_parent.select())
        tab = self.tabs[tab_index]

        note_title = tab.currentNote.get()
        if note_title:
            try:
                self.mb.bookmark(note_title)
            except Exception as e:
                error_msg = f'Exception occurred when trying to bookmark Note: {str(e)}'
                log.error(error_msg)
                showerror("Bookmarking failed!", error_msg)
                return

            self.update_menu()  # Technically only need to update bookmark menu
        else:
            showwarning("Bookmarking failed!", "Can only bookmark notes!")
            log.warning("Tried to bookmark tab that isn't a Note.")

    def _set_tabs(self):
        open_notes = []
        for tab in self.tabs:
            if type(tab) is NoteTab:
                note_title = tab.currentNote.get()
                if note_title:
                    open_notes.append(note_title)
        try:
            self.mb.set_tabs(open_notes)
        except Exception as e:
            error_msg = f'Exception occurred when trying to save tabs: {str(e)}'
            log.error(error_msg)
            showerror("Saving tabs failed!", error_msg)
            return

    def switch_to_workspace(self, workspace_title):
        # Check if any unsaved contents
        if any(tab.unsaved_content for tab in self.tabs if type(tab) is NoteTab):
            confirmation = askyesno(title='confirmation',
                message='Warning: Unsaved changes will be lost when switching to another workspace! Are you sure you want to switch?')
            # Don't switch unless the user confirms
            if not confirmation:
                return

        # Remember tabs
        self._set_tabs()

        # Set active workspace in MB
        self.mb.select_workspace(workspace_title)

        # Display the new workspace
        self._load_workspace()

    def quit(self, event=None):
        confirmation = askyesno(title='confirmation',
            message='Are you sure that you want to quit?')
        if confirmation:
            self.window.quit()

            # Make sure that all the tab and bookmark info is stored
            self._set_tabs()

    def _load_workspace(self):
        self.update_menu()

        # Close all tabs in case any are open
        for _ in range(len(self.tabs)):
            self.tab_parent.forget(0)
            self.tabs.pop(0)

        # Add tabs
        self.creation_tab()
        note_titles = self.mb.get_tabs()
        if note_titles is not None:
            for title in note_titles:
                self.new_tab()
                self.tabs[-1].do(action='open', note_title=title)
        else:
            self.new_tab()

        self._set_title()

    def display(self):
        self.tab_parent = CustomNotebook(self.close_tab, self.window)

        # Add menus that are common to all tabs
        self.menu = tk.Menu(self.window)
        self.window.config(menu=self.menu)

        self.systemMenu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label="System", menu=self.systemMenu)
        self.systemMenu.add_command(label="Quit", command=self.quit)
        self.window.protocol('WM_DELETE_WINDOW', self.quit)

        self.tabMenu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label='Tabs', menu=self.tabMenu)
        self.tabMenu.add_command(label='New tab', command=self.new_tab)
        self.tabMenu.add_command(label='Close tab', command=self.close_tab)
        self.tabMenu.add_command(label='Close all tabs', command=self.close_all_tabs)

        # Menu for workspaces
        self.workspaceMenu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label='Workspaces', menu=self.workspaceMenu)

        # Menu for bookmarks
        self.bookmarkMenu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label='Bookmarks', menu=self.bookmarkMenu)

        # Menu for recent Notes
        self.recentMenu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label='Recent', menu=self.recentMenu)

        self._load_workspace()

        # Note-independent navigation
        navigationFrame = tk.Frame(self.window, padx=5, pady=10)
        # Back button
        backButton = tk.Button(navigationFrame, text='Back')
        backButton.pack(side=tk.LEFT)
        backButton.config(command=partial(self.do, action='back'))

        # Load button
        loadButton = tk.Button(navigationFrame, text='Load')
        loadButton.pack(side=tk.LEFT)
        loadButton.config(command=self._open_note)

        # Search field
        self.searchButton = tk.Button(navigationFrame, text='Search')
        self.searchButton.pack(side=tk.RIGHT)
        self.searchEntry = tk.Entry(navigationFrame)
        self.searchEntry.pack(side=tk.RIGHT)
        self.searchButton.config(command=partial(self.do, action='search',
            query=self.searchEntry))
        navigationFrame.pack(side=tk.TOP, fill=tk.X)

        self.tab_parent.pack(expand=1, fill='both')
        self.tab_parent.bind('<<NotebookTabChanged>>', self.on_tab_selected)

    def _open_note(self):
        user_input = simpledialog.askstring(title="Open Note",
                        prompt="Enter the title of the Note to open:")
        if user_input:
            if self.mb[user_input] is not None:
                self.do(note_title=user_input, action="open")
            else:
                showwarning(title="Failed to open Note",
                    message=f"There is no Note with title '{user_input}'")

    def on_tab_selected(self, event=None):
        #TODO Ideally ask for confirmation if switching away from tab with unsaved changes
        # as we want to reload when coming back
        selected_tab = event.widget.select()
        tab_index = self.tab_parent.index(selected_tab)

        # Special tab to create new tabs is on the very left
        if tab_index == 0:
            self.new_tab()

    def add_tab(self, tab, name):
        self.tabs.append(tab)
        self.tab_parent.add(tab.frame, text=name)

    def new_tab(self, event=None):
        if self.links_in_new_frames:
            self.add_tab(
                NoteTab(
                    self.tab_parent,
                    mb=self.mb,
                    menu_callback=self.update_menu,
                    parent_action_callback=self.do,
                    width=self.width,
                    height=self.height,
                    open_frame_callback=self.open_frame,
                ),
                name='New',
            )
        else:
            self.add_tab(
                NoteTab(
                    self.tab_parent,
                    mb=self.mb,
                    width=self.width,
                    height=self.height,
                    menu_callback=self.update_menu,
                    parent_action_callback=self.do,
                ),
                name='New',
            )
        self.tab_parent.select(self.tab_parent.tabs()[-1])

    def creation_tab(self, event=None):
        self.add_tab(CreationTab(self.tab_parent), name='+')

    def open_frame(self, event=None, note_title=None):
        # If Note is already open, switch to tab instead
        for i, tab in enumerate(self.tabs):
            if type(tab) is NoteTab and tab.currentNote.get()==note_title:
                self.tab_parent.select(i)
                return

        self.new_tab(event=event)  # This also focuses the new tab
        self.do(action='open', note_title=note_title)

    def close_all_tabs(self, event=None):
        confirmation = askyesno(title='confirmation',
            message='Are you sure you want to close all tabs? (Unsaved changes might be lost.)')
        if confirmation:
            for _ in range(len(self.tabs)):
                self.tab_parent.forget(0)
                self.tabs.pop(0)

            # Open one new tab since we can't do anything without a tab
            self.creation_tab()
            self.new_tab()

    def close_tab(self, event=None, tab_index=None):
        if tab_index is None:
            # Get index of currently active tab
            tab_index = self.tab_parent.index(self.tab_parent.select())

        if type(self.tabs[tab_index]) is CreationTab:
            return  # Can't close creation tab

        # If the tab has unsaved changes, we ask for confirmation,
        # otherwise we just close it
        if self.tabs[tab_index].unsaved_content:
            confirmation = askyesno(title='confirmation',
                message='Are you sure that you want to close the tab? (Unsaved changes)')
            if confirmation:
                self.tab_parent.forget(tab_index)
                self.tabs.pop(tab_index)
        else:
            self.tab_parent.forget(tab_index)
            self.tabs.pop(tab_index)


class CreationTab:

    def __init__(self, tab_parent):
        self.frame = ttk.Frame(tab_parent)


class NoteContent(ttk.Frame):

    def __init__(self, *args, parent, **kwargs):
        super().__init__(*args, **kwargs)

        self.parent = parent
        self.link_registry = {}
        self.note = None

        self.display()

    def _make_ref(self, note_title):
        link_text = REFERENCE_STRING % note_title
        self.link_registry[link_text] = note_title
        return link_text

    def _replace_refs(self, text):
        make_ref_fct = lambda res: self._make_ref(res.group(1))
        return re.sub(REFERENCE_REGEX, make_ref_fct, text)

    def load(self, note: Note):
        # For some reason many notes have '\r\n' for newline
        # which causes displaying issues
        content = note['content'].replace('\r\n', '\n').replace('\r', '\n')

        datetext = "(Created: " + note.metadata['creation'].strftime(os.getenv("DATETIME_FORMAT"))
        datetext += ", changed: " + note.metadata['lastchange'].strftime(os.getenv("DATETIME_FORMAT")) + ")"
        self.dates_var.set(datetext)

        # Process content to parse references
        content = self._replace_refs(content)

        # Delete Content
        self.textArea.delete('1.0', tk.END)
        # Reset tags
        for tag in self.textArea.tag_names():
            self.textArea.tag_remove(tag, '1.0', 'end')

        # Set Content
        self.textArea.insert('1.0', content)

        for link in self.link_registry:
            # Search based on https://stackoverflow.com/a/29316232
            idx = '1.0'
            while idx:
                idx = self.textArea.search(link, idx, nocase=0, stopindex=tk.END)
                if idx:
                    lastidx = f'{idx}+{len(link)}c'
                    self.textArea.tag_add('link', idx, lastidx)
                    idx = lastidx

        self.note = note

    def get_note_dict(self):
        content = self.textArea.get('1.0', tk.END)[:-1]
        # textArea is adding a newline at the end which we don't want

        return {"content": content}

    def display(self):
        # Editor (main area)
        self.scrollBar = ttk.Scrollbar(self)
        self.textArea = tk.Text(self, font="Calibri 15",
            relief=tk.FLAT, yscrollcommand=self.scrollBar.set)
        self.scrollBar.config(command=self.textArea.yview)
        self.scrollBar.pack(side=tk.RIGHT, fill=tk.Y)
        self.textArea.bind("<Key>", self.parent.keyDown)
        self.textArea.tag_config("link", foreground=os.getenv("LINK_COLOR"))
        self.textArea.tag_bind("link", "<Button-1>", self.link_callback)

        # Other Note metadata
        self.dates_var = tk.StringVar()
        dateText = tk.Label(self, textvariable=self.dates_var)
        dateText.pack(side=tk.BOTTOM)

        self.textArea.pack(side=tk.LEFT, fill=tk.BOTH)

    def link_callback(self, event):
        """Function to call when links are clicked

        Based on https://stackoverflow.com/a/40940946"""

        # get the index of the mouse click
        index = event.widget.index("@%s,%s" % (event.x, event.y))

        # get the indices of all "link" tags
        tag_indices = list(event.widget.tag_ranges('link'))

        # iterate them pairwise (start and end index)
        for start, end in zip(tag_indices[0::2], tag_indices[1::2]):
            # check if the tag matches the mouse click index
            if event.widget.compare(start, '<=', index) and event.widget.compare(index, '<', end):
                text = event.widget.get(start, end)

                if text in self.link_registry:
                    if self.parent.open_frame_callback is not None:
                        self.parent.open_frame_callback(event=event, note_title=self.link_registry[text])
                    else:
                        self.parent.do(event=event, action='open', note_title=self.link_registry[text])


class SearchContent(NoteContent):

    def load(self, query: str | None, **kwargs):
        if query:
            notes = self.parent.mb.search_by_query(query=query)
        else:
            notes = self.parent.mb.search(**kwargs)

        # Display the results
        if len(notes)>0:
            if query:
                content = f'Search for "{query}"\n\n'
            else:
                content = ''
            content += f"Found {len(notes)} Notes:\n"
            for note in notes:
                content += '\n- ' + REFERENCE_STRING % note['title']
        else:
            content = "Nothing found."

        # Process content to parse references
        content = self._replace_refs(content)

        # Set Content
        self.textArea.insert('1.0', content)

        for link in self.link_registry:
            # Search based on https://stackoverflow.com/a/29316232
            idx = '1.0'
            while idx:
                idx = self.textArea.search(link, idx, nocase=0, stopindex=tk.END)
                if idx:
                    lastidx = f'{idx}+{len(link)}c'
                    self.textArea.tag_add('link', idx, lastidx)
                    idx = lastidx

    def display(self):
        # Editor (main area)
        self.scrollBar = ttk.Scrollbar(self)
        self.textArea = tk.Text(self, font="Calibri 15",
            relief=tk.FLAT, yscrollcommand=self.scrollBar.set)
        self.scrollBar.config(command=self.textArea.yview)
        self.scrollBar.pack(side=tk.RIGHT, fill=tk.Y)
        self.textArea.bind("<Key>", self.parent.keyDown)
        self.textArea.tag_config("link", foreground=os.getenv("LINK_COLOR"))
        self.textArea.tag_bind("link", "<Button-1>", self.link_callback)

        self.textArea.pack(side=tk.LEFT, fill=tk.BOTH)


class NoteTab:

    def __init__(
            self,
            tab_parent,
            menu_callback,
            parent_action_callback,
            mb: MB,
            width: int,
            height: int,
            max_title_chars: int=15,
            open_frame_callback=None,
        ):
        self.mb = mb
        self.open_frame_callback = open_frame_callback

        self.history = []

        self.frame = ttk.Frame(tab_parent)
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_rowconfigure(0, weight=1)
        self.frame.grid_rowconfigure(1, weight=5)
        self.frame.grid_rowconfigure(2, weight=1)
        self.frame.grid_rowconfigure(3, weight=1)

        # Note title
        self.currentNote = tk.StringVar(self.frame, None)
        self.titleEntry = tk.Entry(self.frame, textvariable=self.currentNote)
        self.titleEntry.config(state='readonly')
        self.titleEntry.grid(row=0, column=0, columnspan=2, sticky=tk.E+tk.W)

        # Note type
        self.type_var = tk.StringVar()
        typeFrame = tk.Frame(self.frame)
        typeText = tk.Label(typeFrame, text='Type:')
        typeText.pack(side=tk.LEFT)
        typeEntry = tk.Entry(typeFrame, textvariable=self.type_var)
        typeEntry.pack(side=tk.RIGHT)
        typeFrame.grid(row=0, column=2, columnspan=2, sticky=tk.E)
        typeEntry.bind("<Key>", self.keyDown)

        # Note contents
        self.contents = NoteContent(self.frame, parent=self)
        self.contents.grid(row=1, column=0, columnspan=4, sticky=tk.N+tk.S)

        # Save and delete buttons
        saveButton = tk.Button(self.frame, text='Save')
        saveButton.grid(row=3, column=0)
        saveButton.config(command=partial(self.do, action='save'))
        deleteButton = tk.Button(self.frame, text='Delete')
        deleteButton.grid(row=3, column=3)
        deleteButton.config(command=partial(self.do, action='delete'))

        # Related notes
        relatedFrame = tk.Frame(self.frame)
        relatedText = tk.Label(relatedFrame, text='Incoming:')
        relatedText.pack(side=tk.LEFT)
        self.relatedOption = tk.StringVar()
        self.relatedMenu = tk.OptionMenu(relatedFrame, self.relatedOption, '')
        self.relatedMenu.pack(side=tk.RIGHT)
        relatedFrame.grid(row=3, column=1, columnspan=2)

        # Keep a reference to the parent, so we can change the tab title
        self.parent = tab_parent
        self.menu_callback = menu_callback  # For updating the menu
        self.parent_action = parent_action_callback  # For doing actions in the overall notebook
        self.max_title_chars = max_title_chars

        self.unsaved_content = False

        self.frame.place(x=0, y=0, width=width, height=height)

    def update_title(self):
        note_title = self.currentNote.get()
        if note_title:
            # For "shortened" titles add '...'
            if len(note_title) > self.max_title_chars-3:
                note_title = note_title[:self.max_title_chars-3] + '...'
            title = note_title

            if self.unsaved_content:
                title += '*'
        else:
            title = '*'
        self.parent.tab(self.frame, text=title)

    def update_related_menu(self):
        # First reset the menu
        self.relatedOption.set('')
        self.relatedMenu['menu'].delete(0, 'end')

        if self.currentNote.get():
            links = self.mb.search_links(note_B=self.currentNote.get())
            for link in links:
                title = link.A
                self.relatedMenu['menu'].add_command(label=title, command=partial(self.parent_action, action='open', note_title=title))
            self.relatedOption.set(f'{len(links)} Note{"s" if len(links)!=1 else ""}')

    def _clear_contents(self):
        for widget in self.contents.winfo_children():
            widget.destroy()
        self.contents.destroy()

    def _action_back(self, event, note_title):
        if len(self.history)>0:
            last_title = self.history.pop()

            #FIXME There are cases where this leads to some
            # inconsistencies (nothing major though)
            # - Clicking back and not confirming still changes the history
            if last_title==self.currentNote.get() and len(self.history)>0:
                last_title = self.history.pop()

            self.do(event=event, action='open', note_title=last_title)

    def _action_open(self, event, note_title):
        # If there are unsaved changes, we want to confirm before loading anything
        if self.unsaved_content:
            confirmation = askyesno(title='confirmation',
                message='Are you sure you want to go to the other Note? (Unsaved changes)')
            if not confirmation:
                return False

        self.currentNote.set(note_title)

        content = ''
        if note_title:
            note = self.mb[note_title]

            if note is not None:
                note_type = note.metadata['type']
                self.type_var.set(note_type)

                # Reload the content elements
                # (Note that we might want to switch to another content class!)
                self._clear_contents()
                if note.gui_class:
                    # Switch to type-specific content class if any is registered
                    self.contents = note.gui_class(self.frame, parent=self)
                else:
                    self.contents = NoteContent(self.frame, parent=self)
                self.contents.grid(row=1, column=0, columnspan=4, sticky=tk.N+tk.S)
                self.contents.load(note=note)

            else:
                warning_msg = f'Could not find Note with title "{note_title}"!'
                logging.warning(warning_msg)
                showwarning("Note not found!", warning_msg)

        if not note_title or note is None:
            # Reset all contents and load the default content class
            self._clear_contents()
            self.contents = NoteContent(self.frame, parent=self)
            self.contents.grid(row=1, column=0, columnspan=4, sticky=tk.N+tk.S)

        # Set Title
        self.unsaved_content = False
        self.update_title()
        self.update_related_menu()

        if len(self.history)<1 or self.history[-1]!=self.currentNote.get():
            if self.currentNote.get() is not None:
                self.history.append(self.currentNote.get())

        return True

    def _action_save(self, event, note_title):
        note_dict = self.contents.get_note_dict()
        note_dict["note_type"] = self.type_var.get()
        #TODO if the note type is changed, make sure it only contains allowed characters

        if note_title:
            # First read the Note
            note = self.mb[note_title]

            # Save the changes to MB
            try:
                note_title = self.mb.edit_note(note_title, **note_dict)["title"]
                if note_title:
                    log.info(f'Saved changes for Note with title "{note_title}"')
                    self.unsaved_content = False
                else:
                    error_msg = f'Could not save changes for Note with title "{note_title}"!'
                    log.error(error_msg)
                    showerror("Saving failed!", error_msg)

                    return False
            except Exception as e:
                error_msg = f'Exception occurred when trying to save Note: {str(e)}'
                log.error(error_msg)
                showerror("Saving failed with exception!", error_msg)
                return False

        else:
            note = create_note(note_dict)
            note_title = self.mb.add_note(note)
            self.currentNote.set(note_title)

            if note_title:
                log.info(f'Successfully added new Note with title "{note_title}"')
                self.unsaved_content = False
            else:
                log.error(f'Could not save status!')
                return False

        # Reloading the Note since parsing might have changed the contents
        self.do(action='open', note_title=note_title)

        # Update the menu
        self.menu_callback()

    def _action_delete(self, event, note_title):
        # Special case: Deleting the active workspace
        # We could allow for this with additional changes, but for now
        # we just block it and tell the user to delete it from another workspace
        if note_title==self.mb.active_workspace:
            showerror("Can't delete Note!", "Error: It is not possible to delete the currently active workspace!\n"\
                "If you want to delete this workspace, please open the Note from another workspace.")
            return

        confirmation_msg = f'Are you sure you want to delete the Note with title "{note_title}"?'
        num_incoming_links = len(self.mb.search_links(note_B=note_title))
        if num_incoming_links>0:
            confirmation_msg += f'\n\nWARNING: There are {num_incoming_links} other Notes linking to this one!'
        confirmation = askyesno(title='confirmation', message=confirmation_msg)
        if confirmation:
            try:
                if self.mb.delete_note(note_title):
                    log.info(f'Deleted Note with title "{note_title}"')
                    #TODO could also show user message

                    # Go to home page if we deleted the current Note
                    if note_title == self.currentNote.get():
                        self.do(action='open')
                else:
                    error_msg = f'Could not delete Note with title "{note_title}"!'
                    log.error(error_msg)
                    showerror("Deletion failed!", error_msg)
            except Exception as e:
                error_msg = f'Exception occurred when trying to delete Note: {str(e)}'
                log.error(error_msg)
                showerror("Deletion failed with exception!", error_msg)
                return False

        # Update the menu
        self.menu_callback()

    def _action_search(self, event, query, **kwargs):
        # If there are unsaved changes, we want to confirm before switching
        # to search
        if self.unsaved_content:
            confirmation = askyesno(title='confirmation',
                message='Are you sure you want to go to search? (Unsaved changes)')
            if not confirmation:
                return False

        # Reload the content elements
        # (Note that we want to switch to another content class!)
        self._clear_contents()
        self.contents = SearchContent(self.frame, parent=self)
        self.contents.grid(row=1, column=0, columnspan=4, sticky=tk.N+tk.S)
        self.contents.load(query=query, **kwargs)

        # Adjust other contents
        self.type_var.set('')
        self.titleEntry.delete(0, tk.END)
        self.currentNote.set('')
        self.update_title()
        self.update_related_menu()
        self.unsaved_content = False

    def do(self, event=None, action=None, note_title=None, query=None, **kwargs):
        if action == 'back':
            return self._action_back(event=event, note_title=note_title)

        elif action == 'open':
            return self._action_open(event=event, note_title=note_title)

        elif action == 'save':
            if note_title is None:
                note_title = self.currentNote.get()

            return self._action_save(event=event, note_title=note_title)

        elif action == 'delete':
            if note_title is None:
                note_title = self.currentNote.get()

            return self._action_delete(event=event, note_title=note_title)

        elif action == 'search':
            return self._action_search(
                event=event,
                query=query.get() if (query and type(query) is not str) else query,
                **kwargs,
            )

    def keyDown(self, event=None):
        if not self.unsaved_content and \
            (event.char or event.keysym in ['BackSpace', 'Return']):
            self.unsaved_content = True
            self.update_title()

