import datetime
import re
import os
import dateutil.parser
import json
import logging
import difflib
from typing import List, Dict

from .links import LinkType, Link
from .aes_crypto import AESCipher


log = logging.getLogger(__name__)

DATETIME_FORMAT = "%b %d %Y %H:%M:%S"  # for storing and loading (i.e. backend)
DEFAULT_NOTE_TYPE = "note"
DEFAULT_WORKSPACE = "default"
DELETED_NOTE_TITLE = "DELETED NOTE"
ENCODING = 'utf-8'

DISALLOWED_CHARS = r'\[\]\{\}'
REFERENCE_REGEX = rf'\[\[([^{DISALLOWED_CHARS}]+)\]\]'
REFERENCE_STRING = "[[%s]]"
MAX_TITLE_LENGTH = 100  # in characters

CREATION_REGEX = r"(?P<type>[a-zA-Z#@]+){{(([^\}\{]|[\s]\[([^\}\{])*\])+)}}"

NOTE_TYPE_REGISTRY = {}  # note type: Subclass of Note
# To be populated by modules implementing special note types


def sanitize(title: str):
    """Sanitize a title to not include any problematic characters"""
    # Remove invalid characters and limit length
    filename = re.sub(rf'[{DISALLOWED_CHARS}]', "_", title)
    return filename.strip()

def title_from_content(text: str):
    first_line = text.split('\n')[0].strip()
    return sanitize(first_line)[:MAX_TITLE_LENGTH].strip()


class Note():
    '''Class for storing and processing Notes.'''

    gui_class = None  # Set in subclasses to use type-specific displaying

    def __init__(self,
        content='',
        tags=[],
        kwtags={},
        creation=None,
        lastchange=None,
        note_type=None,
        **kwargs
    ):
        '''Create a new Note.

        Params:
        :param content: content as string (using markdown for formatting)
        :param tags: list of tags, e.g. ['psychology', 'life', 'politics']
        :param kwtags: list of keyword tags, e.g. {'sentiment': 'positive'}'''
        if creation is None:
            creation = datetime.datetime.now()
        if type(creation) is str:
            creation = datetime.datetime.strptime(creation, DATETIME_FORMAT)
        if lastchange is None:
            lastchange = datetime.datetime.now()
        if type(lastchange) is str:
            lastchange = datetime.datetime.strptime(lastchange, DATETIME_FORMAT)
        if not note_type:
            note_type = DEFAULT_NOTE_TYPE

        # initialize dictionary for all the data
        self.data = {}
        self.metadata = {}

        # set initial content
        self.data['content'] = content.strip()

        for keyword,value in kwargs.items():
            self.metadata[keyword] = value
        self.metadata['creation'] = creation
        self.metadata['type'] = note_type

        # update tag and kwtags based on the content, set lastchange
        self.update(lastchange)

        # add the new tags
        for tag in tags:
            if tag not in self.data['tags']:
                self.data['content'] += ' #%s'%tag
        for keyword,tag in kwtags.items():
            if keyword not in self.data['kwtags'].keys():
                self.data['content'] += ' #%s:%s'%(keyword,tag)
            elif self.data['kwtags'][keyword]!=tag:
                self.data['content'] += ' #%s:%s'%(keyword,tag)

        # update tag and kwtags again
        self.update(lastchange)


    def update(self, lastchange=None):
        '''Parse the content to update the tags and kwtags.

        Note that relations are updated by MB.'''
        tags = [tag[0].strip() for tag in re.findall(r"#((?P<title>[\w-]+)(:(?P<value>[\w#!@-]+))?([\s]+|$))",
                                                     self.data['content'])]
        self.data['tags'] = set([])
        self.data['kwtags'] = {}
        for tag in tags:
            t = tag.split(':')
            if len(t)==1:
                self.data['tags'].add(tag)
            elif len(t)==2:
                self.data['kwtags'][t[0]] = t[1]

        if lastchange is None:
            self.metadata['lastchange'] = datetime.datetime.now()
        else:
            self.metadata['lastchange'] = lastchange

    def edit(self, content):
        self.data['content'] = content.lstrip()
        self.update()

    def __getitem__(self, key):
        if key=='title':
            return title_from_content(self['content'])
        return self.data[key]

    def __setitem__(self, key, val):
            self.data[key] = val

    def __dir__(self):
        return self.data

    def __repr__(self):
        return self.data.__repr__()

    def keys(self):
        return self.data.keys()

    def get_links(self) -> List[Link]:
        """Get a list of links going out of this Note."""
        links = []

        references = re.findall(REFERENCE_REGEX, self['content'])
        for reference in references:
            links.append(
                Link(
                    in_note=self['title'],
                    out_note=reference,
                    link_type=LinkType.CONTENT,
                )
            )
        return links

    def update_references(self, title_changes: Dict[str, str | None]):
        """Take a dictionary with title changes and update all affected references
        in this Note.

        Arguments:
        title_changes -- dict with old titles as keys and new titles as values.
            For notes that were deleted, pass None as new title."""
        for old_title, new_title in title_changes.items():
            original_ref = REFERENCE_STRING % old_title
            if new_title is not None:
                new_ref = REFERENCE_STRING % new_title
            else:
                new_ref = REFERENCE_STRING % DELETED_NOTE_TITLE

            self.data['content'] = self.data['content'].replace(original_ref, new_ref)

    def to_dict(self):
        """Convert the Note to a dictionary.

        The dictionary that is returned can be used to initialize the same Note again
        by passing it as arguments for creation: Note(**self.to_dict())."""
        # content
        note_dict = {}
        note_dict['content'] = self.data['content']
        # Note that tags and kwtags can be inferred from content

        # note_type, kwargs, creation, lastchange
        for key,value in self.metadata.items():
            if key!='type':
                if key=='creation' or key=='lastchange':
                    # convert datetime to string so everything is JSON serializable
                    note_dict[key] = datetime.datetime.strftime(value, DATETIME_FORMAT)
                else:
                    note_dict[key] = value
            else:
                note_dict['note_type'] = value
        return note_dict


class Workspace(Note):

    name = "workspace"

    def update_references(self, title_changes: Dict[str, str | None]):
        super().update_references(title_changes=title_changes)

        deleted_notes = set([title for title, new_title in title_changes.items() if new_title is None])
        tabs = [title if title not in title_changes else title_changes[title]
            for title in self.get_tabs() if title not in deleted_notes]
        bookmarks = [title if title not in title_changes else title_changes[title]
            for title in self.get_bookmarks() if title not in deleted_notes]

        self.set_tabs(tabs)
        self.metadata["bookmarks"] = bookmarks

    def get_links(self) -> List[Link]:
        """Get a list of links going out of this Note."""
        links = super().get_links()

        # Get references from tabs and bookmarks
        for reference in self.get_tabs() + self.get_bookmarks():
            link = Link(in_note=self["title"], out_note=reference, link_type=LinkType.METADATA)
            if link not in links:
                links.append(link)

        return links

    def get_tabs(self):
        if "tabs" in self.metadata:
            return self.metadata["tabs"]
        else:
            return []

    def set_tabs(self, tabs: List[str]):
        self.metadata["tabs"] = tabs

    def get_bookmarks(self):
        if "bookmarks" in self.metadata:
            return self.metadata["bookmarks"]
        else:
            return []

    def bookmark(self, note_title):
        if "bookmarks" in self.metadata:
            if note_title not in self.metadata["bookmarks"]:
                self.metadata["bookmarks"].append(note_title)
            else:
                self.metadata["bookmarks"].remove(note_title)
        else:
            self.metadata["bookmarks"] = [note_title]

NOTE_TYPE_REGISTRY[Workspace.name] = Workspace


def create_note(note_dict):
    """Create a Note from the given dict and return the new Note.

    This function infers the appropriate class to use
    (which is either Note or a subclass of Note)."""
    note_type = note_dict["note_type"]

    if note_type in NOTE_TYPE_REGISTRY:
        note_cls = NOTE_TYPE_REGISTRY[note_type]
    else:
        note_cls = Note

    return note_cls(**note_dict)


class IntegrityError(Exception):
    pass


class MB:
    def __init__(self, path, key_path, enable_log=None, log_kwargs={}):
        """Initialize the organizer.

        Arguments:
        path -- The path to the organizer directory
        key_path -- The path to the key file
        enable_log -- If True, each change to note contents is written to the organizer history
          (This includes contents of deleted files, so it can have security implications)
          If None, read the property from the savefile.
        log_kwargs -- Any additional keyword arguments that are relevant for logging
        """
        self.paths = {
            "dir": path,
            "history": os.path.join(path, "history.encr"),
            "metadata": os.path.join(path, "metadata.encr"),
        }
        self.key_path = key_path

        self.enable_log = enable_log
        self.log_kwargs = log_kwargs

        # Initialize decryption
        self.ciph = AESCipher(key_path=self.key_path)

        # Make sure the organizer directory exists
        if not os.path.isdir(self.paths["dir"]):
            log.info(f"Creating organizer directory at {self.paths['dir']} ...")
            os.makedirs(self.paths["dir"])

        # Load contents from the directory
        self.read_notes_from_disk()
        self.read_history_from_disk()
        self.read_metadata_from_disk()

    def _read_note(self, note_title):
        fpath = os.path.join(self.paths["dir"], self.note_files[note_title])

        with open(fpath, 'rb') as f:
            encrypted_note = f.read()
        json_note = json.loads(self.ciph.decrypt(encrypted_note))
        return create_note(json_note)

    def read_notes_from_disk(self):
        # Initialize note-related attributes
        self.notes = {}  # title: note
        self.tags = set([])
        self.kwtags = set([])
        self.links = set([])
        self.note_files = {}
        self.next_file_number = 0

        # Read notes from organizer directory
        for fname in os.listdir(self.paths["dir"]):
            fpath = os.path.join(self.paths["dir"], fname)

            # We only consider files with names NUM.encr
            if os.path.isfile(fpath) and re.match(r"^\d+\.encr$", fname):
                # Read the note from the file
                with open(fpath, 'rb') as f:
                    encrypted_json = f.read()
                json_status = self.ciph.decrypt(encrypted_json)

                json_note = json.loads(json_status)
                note = create_note(json_note)

                # Add the note
                self.add_note(note, file_exists=True)

                # Remember which file belongs to the note
                self.note_files[note['title']] = fname

                # Keep track of the highest number already in use
                # so we know which note file to create next
                file_number = int(fname.split('.')[0])
                self.next_file_number = max(self.next_file_number, file_number + 1)
        log.info(f"Read {len(self.notes)} notes from disk.")

    def read_history_from_disk(self):
        self.history = []

        # Read history from file if it exists
        if os.path.isfile(self.paths["history"]):
            with open(self.paths["history"], 'rb') as f:
                for line in f:
                    if len(line.strip())<1:
                        continue

                    json_status = self.ciph.decrypt(line)
                    hlog = json.loads(json_status)
                    self.history.append(hlog)
        log.info(f"Read {len(self.history)} log entries from disk.")

    def read_metadata_from_disk(self):
        self.active_workspace = None  # note title

        # Read metadata from file if it exists
        if os.path.isfile(self.paths["metadata"]):
            with open(self.paths["metadata"], 'rb') as f:
                encrypted_json = f.read()
                json_status = self.ciph.decrypt(encrypted_json)
                metadata = json.loads(json_status)

                if "active_workspace" in metadata:
                    self.active_workspace = metadata["active_workspace"]
                if "enable_log" in metadata and self.enable_log is None:
                    self.enable_log = metadata["enable_log"]

        # Ensure that all metadata attributes are set
        if self.enable_log is None:
            self.enable_log = False
        self._ensure_workspace()

    def _ensure_workspace(self):
        if len(self.get_workspaces())<1:
            log.info("No workspace exist yet. Creating default workspace.")
            self.add_note(Workspace(content=DEFAULT_WORKSPACE, note_type=Workspace.name))
        if self.active_workspace is None:
            self.active_workspace = self.get_workspaces()[0]

    def _verify_integrity(self, note_title):
        note = self.notes[note_title]
        stored_note = self._read_note(note_title=note_title)

        # We might have changed various aspects of the note,
        # but the creation time should still be the same and
        # can serve as a fingerprint here.
        # NOTE: This isn't perfect because we usually don't store microseconds
        # and when creating several inline notes they can have the same timestamp.
        # (However, that case is sufficiently rare.)
        if stored_note.metadata["creation"]==note.metadata["creation"].replace(microsecond=0):
            return True
        else:
            raise IntegrityError(f"File of note '{note_title}' has an unexpected creation time!")

    def _save_note(self, note_title):
        note = self.notes[note_title]
        fpath = os.path.join(self.paths["dir"], self.note_files[note_title])

        encrypted_note = self.ciph.encrypt(json.dumps(note.to_dict()).encode(ENCODING))
        with open(fpath, 'wb') as f:
            f.write(encrypted_note)

    def _save_metadata(self):
        metadata = {
            "active_workspace": self.active_workspace,
            "enable_log": self.enable_log,
        }
        with open(self.paths["metadata"], 'wb') as f:
            encrypted_json = self.ciph.encrypt(json.dumps(metadata).encode(ENCODING))
            f.write(encrypted_json)

    def select_workspace(self, note_title):
        self.active_workspace = note_title
        self._save_metadata()

    def get_workspaces(self, **kwargs):
        """Returns a list of all notes which can be used as workspace"""
        notes = self.search(note_type=Workspace.name, **kwargs)
        return [note["title"] for note in notes]

    def bookmark(self, note_title):
        # Verify integrity of note file (raises exception in case of issues)
        self._verify_integrity(note_title=self.active_workspace)

        workspace = self[self.active_workspace]

        assert type(workspace) is Workspace, f"Note '{workspace['title']}' isn't of type Workspace!"
        workspace.bookmark(note_title)

        # Update references
        self.extract_links(workspace)

        # Store changes
        self._save_note(self.active_workspace)

    def get_bookmarks(self, workspace=None):
        if workspace is None:
            workspace = self[self.active_workspace]

        assert type(workspace) is Workspace, f"Note '{workspace['title']}' isn't of type Workspace!"
        return workspace.get_bookmarks()

    def get_tabs(self, workspace=None):
        if workspace is None:
            workspace = self[self.active_workspace]

        assert type(workspace) is Workspace, f"Note '{workspace['title']}' isn't of type Workspace!"
        return workspace.get_tabs()

    def set_tabs(self, tabs):
        # Verify integrity of note file (raises exception in case of issues)
        self._verify_integrity(note_title=self.active_workspace)

        workspace = self[self.active_workspace]
        assert type(workspace) is Workspace, f"Note '{workspace['title']}' isn't of type Workspace!"
        workspace.set_tabs(tabs)

        # Update references
        self.extract_links(workspace)

        # Store changes
        self._save_note(self.active_workspace)

    def log(self, name, **kwargs):
        if self.enable_log:
            data = {}
            data.update(self.log_kwargs)
            now = datetime.datetime.strftime(datetime.datetime.now(), DATETIME_FORMAT)
            data.update({"datetime": now, "action": name, "info": kwargs})
            self.history.append(data)

            # Write new entry to disk as well
            with open(self.paths["history"], 'ab') as f:
                encrypted_json = self.ciph.encrypt(json.dumps(data).encode(ENCODING))
                f.write(encrypted_json + "\n".encode(ENDOGING))

    def get_log(self, from_time=None, to_time=None, actions=None):
        if type(from_time) is str:
            #from_time = datetime.datetime.strptime(from_time, DATETIME_FORMAT)
            from_time = dateutil.parser.parse(from_time)
        if type(to_time) is str:
            #to_time = datetime.datetime.strptime(to_time, DATETIME_FORMAT)
            to_time = dateutil.parser.parse(to_time)

        history = []
        for entry in self.history:
            if actions is not None and entry["action"] not in actions:
                continue
            # We use dateutil for parsing rather than strptime with DATETIME_FORMAT
            # in case the format changed at any point
            entry_time = dateutil.parser.parse(entry["datetime"])
            if from_time is not None and entry_time<from_time:
                continue
            if to_time is not None and entry_time>from_time:
                continue

            history.append(entry)
        return history

    def add_note(self, note, file_exists=False):
        '''Add a Note to the database.

        Note that this function also adds tags to the database if necessary.'''
        title = note['title']
        if self.get_note(title):
            raise Exception(f"Failure to add Note: Note with title '{note['title']}' already existing.")

        # add the Note to the system
        self.notes[title] = note

        if not file_exists:
            # Write to a file and remember the filename here
            self.note_files[title] = f"{self.next_file_number}.encr"
            self._save_note(title)

            self.next_file_number += 1

        # save tags and kwtags if not already existing
        self.tags |= note.data['tags']
        self.kwtags |= set(note.data['kwtags'].keys())

        # parse the Note as well (might create additional Notes)
        self[title]['content'] = self.parse(note['content'])
        content = self[title]['content']

        #FIXME If parsing changes the title, indexing self[title] might fail
        # (Document somewhere that it's not a good idea to create inline notes
        #  from within the title; since this is super edge, not spending time
        #  on fixing it now)

        # update Note relations
        self.extract_links(self[title])

        if not file_exists:
            # Log action
            # (only if the file doesn't already exist, as it does e.g. when
            #  calling it during initialization)
            self.log(name="add", title=note["title"], content=content)

        return note['title']

    def edit_note(self, original_title, content, note_type=None, **kwargs):
        """Edit a Note specified by its original title (i.e. title it had when last saved)

        The Note is edited by updating its content to the given content.
        Tags, keyword tags and relations are updated accordingly."""
        # Verify integrity of note file (raises exception in case of issues)
        self._verify_integrity(note_title=original_title)

        original_content = self[original_title]['content'][:]  # remember for logging

        # It shouldn't be possible to overwrite another note by renaming to it
        if title_from_content(content)!=original_title and title_from_content(content) in self.notes:
            raise ValueError("Note with this title already exists!")

        # for seeing whether any tags or kwtags are removed, we need to check the initial state
        original_tags = self[original_title].data['tags']
        original_kwtags = set(self[original_title].data['kwtags'].keys())

        # the main part of the update is done in the Note class
        self[original_title].edit(self.parse(content))

        # Note type changes are more complicated for note types with special classes
        # as they require us to reload the whole note
        if note_type is not None and note_type!=self[original_title].metadata["type"]:
            note_dict = {
                "content": self[original_title]['content'],
                "note_type": note_type,
                **kwargs
            }
            self.notes[original_title] = create_note(note_dict)
        else:
            for key, value in kwargs.items():
                self[original_title].metadata[key] = value

        # save tags and kwtags if not already existing
        self.tags |= self[original_title].data['tags']
        self.kwtags |= set(self[original_title].data['kwtags'].keys())

        # if tags were removed, we check if any other Notes use them and delete them if not
        removed_tags = original_tags - self[original_title].data['tags']
        removed_kwtags = original_kwtags - (self[original_title].data['kwtags'].keys())
        for removed_tag in removed_tags:
            # remove the tag from the system if no other Note is using it
            if len(self.search(tags=[removed_tag]))==0:
                self.tags = self.tags - set([removed_tag])
        for removed_tag in removed_kwtags:
            # remove the tag from the system if no other Note is using it
            if len(self.search(kwtags={removed_tag:None}))==0:
                self.kwtags = self.kwtags - set([removed_tag])

        title = self[original_title]['title']

        if title != original_title:
            # Update the notes directory
            self.notes[title] = self.notes[original_title]
            del self.notes[original_title]

            # Update filename mapping
            self.note_files[title] = self.note_files[original_title][:]
            del self.note_files[original_title]

            # Also update links to this Note
            for link in self.search_links(note_B=original_title):
                other_note = self[link.A]
                other_note.update_references({original_title: title})

                # Then also update self.links accordingly
                link.B = title

        # update outgoing links
        self._remove_links(note_A=original_title)
        self.extract_links(self[title])

        # Store changes to disk
        self._save_note(title)

        # Log action
        difference = '\n'.join([d for d in difflib.unified_diff(original_content.split('\n'),
            content.split('\n'), n=0)])
        self.log("edit", title=title, change=difference)

        return {"title": title, "change": difference}

    def __getitem__(self, key):
        assert key is not None

        if type(key) is str:
            return self.get_note(key)
        else:
            raise ValueError(f"Can only get Note by title (type 'str'), but key was of type {type(key)}!")

    def get_note(self, title):
        """Get a Note with a specific title."""
        if title in self.notes:
            return self.notes[title]
        else:
            return None

    def get_links(self, note_title):
        links = self.search_links(note_A=note_title)+self.search_links(note_B=note_title)
        return links

    def search_links(self, note_A=None, note_B=None):
        links = list(self.links)

        if note_A is not None:
            links = list(filter(lambda x: x.A==note_A, links))
        if note_B is not None:
            links = list(filter(lambda x: x.B==note_B, links))

        return links

    def _remove_links(self, note_A=None, note_B=None):
        new_links = set()
        for link in self.links:
            if (note_A is None or link.A==note_A) and (note_B is None or link.B==note_B):
                continue
            else:
                new_links.add(link)
        self.links = new_links

    def search_by_query(self, query: str):
        tags = [arg[1:] for arg in query.split() if re.match(r"^#[\w]+$", arg)]
        kwtags = [arg.split(':') for arg in query.split() if re.match(r"^#[\w]+:[\w]*$", arg)]
        # convert to {key: value}, allowing None as value
        kwtags = {x[0][1:]: (x[1] if x[1] else None) for x in kwtags}
        type_val = [arg.split(':')[1] for arg in query.split() if re.match(r"^--type:[\w]+", arg)]
        keywords = [arg for arg in query.split() if not arg.startswith("#") and not arg.startswith('--type:')]
        return self.search(
            must_include=keywords,
            tags=tags,
            kwtags=kwtags,
            note_type=(type_val[0] if len(type_val) else None),
        )

    def search(self, must_include=[], must_not_include=[],
               tags=[], kwtags={}, metadata={}, note_type=None, not_note_types=[],
               creation_time_interval=(None, None),
               lastchange_time_interval=(None, None),
                order_by='lastchange', limit=None):
        results = self.notes.values()
        for keyword in must_include:
            results = list(filter(lambda x: keyword.lower() in x['content'].lower(), results))
        for keyword in must_not_include:
            results = list(filter(lambda x: keyword.lower() not in x['content'].lower(), results))

        # filter by Note type
        if note_type is not None:
            results = list(filter(lambda x: x.metadata['type']==note_type, results))
        if not_note_types:
            results = list(filter(lambda x: x.metadata['type'] not in not_note_types, results))

        # tag-based search
        for tag in tags:
            results = list(filter(lambda x: tag in x.data['tags'], results))
        for key,value in kwtags.items():
            if value is not None:
                results = list(filter(lambda x: key in x.data['kwtags'] and x.data['kwtags'][key]==value, results))
            else:
                results = list(filter(lambda x: key in x.data['kwtags'], results))

        # metadata
        for key,value in metadata.items():
            results = list(filter(lambda x: x.metadata[key]==value, results))

        # Time filters
        if creation_time_interval[0] is not None:
            from_creation = creation_time_interval[0]
            if type(from_creation) is str:
                from_creation = dateutil.parser.parse(from_creation)
            results = list(filter(lambda x: x.metadata['creation']>=from_creation, results))
        if creation_time_interval[1] is not None:
            to_creation = creation_time_interval[1]
            if type(to_creation) is str:
                to_creation = dateutil.parser.parse(to_creation)
            results = list(filter(lambda x: x.metadata['creation']<=to_creation, results))
        if lastchange_time_interval[0] is not None:
            from_lastchange = lastchange_time_interval[0]
            if type(from_lastchange) is str:
                from_lastchange = dateutil.parser.parse(from_lastchange)
            results = list(filter(lambda x: x.metadata['lastchange']>=from_lastchange, results))
        if lastchange_time_interval[1] is not None:
            to_lastchange = lastchange_time_interval[1]
            if type(to_lastchange) is str:
                to_lastchange = dateutil.parser.parse(to_lastchange)
            results = list(filter(lambda x: x.metadata['lastchange']<=to_lastchange, results))

        # Sorting
        if order_by in ['creation', 'lastchange']:
            # Note that we order by most recent first
            results = sorted(results, key=lambda x: x.metadata[order_by])[::-1]
        else:
            log.warning("Invalid argument passed as 'order_by'!")

        if limit is not None:
            return results[:limit]
        else:
            return results

    def delete_note(self, title):
        # Verify integrity of note file (raises exception in case of issues)
        self._verify_integrity(note_title=title)

        note = self.get_note(title)
        content = note["content"][:]  # remember for logging

        # remove tags and kwtags if no other Note uses them
        for tag in note.data['tags']:
            if len(self.search(tags=[tag]))<=1:
                self.tags.remove(tag)
        for key,value in note.data['kwtags'].items():
            if len(self.search(kwtags={key: None}))<=1:
                self.kwtags.remove(key)

        # remove relations
        self._remove_links(note_A=title)
        for link in self.search_links(note_B=title):
            other_note = self[link.A]
            # Update references in linked Notes
            other_note.update_references({title: None})
        self._remove_links(note_B=title)

        # remove the file (Note that we verified integrity above)
        os.unlink(os.path.join(self.paths["dir"], self.note_files[title]))
        del self.note_files[title]

        # remove the Note
        del self.notes[title]

        # Deleting the active workspace requires setting another workspace as active
        if self.active_workspace==title:
            log.warning("Warning: Deleting active workspace! Setting another one.")
            self._ensure_workspace()

        # Log action
        self.log(name="delete", title=title, content=content)

        return True

    def extract_links(self, note):
        """Process Note's content to extract links to other Notes."""
        # We first remove all outgoing relations so it is possible to delete relations
        # (All outgoing relations are defined in the content of the respective Note.)
        self._remove_links(note_A=note['title'])

        links = note.get_links()

        for link in links:
            self.links.add(link)

    def _make_note_from_regex(self, res, creation=None, lastchange=None, **kwtags):
        '''Create a new Note from a regex result.

        Helper function for the main parsing function.'''
        if creation is None:
            creation = datetime.datetime.now()
        if lastchange is None:
            lastchange = datetime.datetime.now()

        type_ = res.group(1)
        content = res.group(2)

        # add the note to the system
        note_dict = {
            "content": content,
            "creation": creation,
            "lastchange": lastchange,
            "note_type": type_,
        }
        for key, value in kwtags.items():
            note_dict[key] = value
        note = create_note(note_dict)
        title = self.add_note(note)

        return REFERENCE_STRING % title

    def parse(self, text, max_iter=100, creation=None, lastchange=None, **kwtags):
        """Parse an input text.

        The main purpose of parsing is to process statements for creating Notes inside other Notes
        when editing or adding contents.

        The syntax for creation is NOTETYPE[contents] (see the regex)."""
        if creation is None:
            creation = datetime.datetime.now()
        if lastchange is None:
            lastchange = datetime.datetime.now()

        for _ in range(max_iter):
            processed_text = re.sub(CREATION_REGEX,
                (lambda x: self._make_note_from_regex(x, creation=creation,
                        lastchange=lastchange, **kwtags)), text)
            if text == processed_text:
                break
            text = processed_text
        return text


