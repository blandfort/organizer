import os
from datetime import datetime
from enum import Enum
import tkinter as tk
from tkinter import ttk

from .system import Note, NOTE_TYPE_REGISTRY, create_note
from .gui import NoteContent


class Frequency(Enum):
    DAILY = "daily"
    WEEKLY = "weekly"
    MONTHLY = "monthly"
    QUARTERLY = "quarterly"
    YEARLY = "yearly"

DEFAULT_FREQUENCY = Frequency.DAILY


def get_current_time_string(frequency: Frequency, time=None) -> str:
    if time is None:
        time = datetime.now()

    if frequency == Frequency.DAILY:
        return time.strftime(os.getenv('FREQUENCY_DAY_FORMAT'))
    elif frequency == Frequency.WEEKLY:
        return time.strftime(os.getenv('FREQUENCY_WEEK_FORMAT'))
    elif frequency == Frequency.MONTHLY:
        return time.strftime(os.getenv('FREQUENCY_MONTH_FORMAT'))
    elif frequency == Frequency.QUARTERLY:
        quarter = (time.month - 1) // 3 + 1
        quarter_format = os.getenv('FREQUENCY_QUARTER_FORMAT').replace('[QUARTER]', str(quarter))
        return time.strftime(quarter_format)
    elif frequency == Frequency.YEARLY:
        return time.strftime("%Y")  # Year, e.g., '2024'
    else:
        raise ValueError("Unsupported frequency")


class JournalPageContent(NoteContent):

    def load(self, note):
        assert type(note) is JournalPage

        super().load(note)

        # Set additional fields
        self.journal.set(note.get_journal())

    def display(self):
        # Editor (main area)
        self.scrollBar = ttk.Scrollbar(self)
        self.textArea = tk.Text(self, font="Calibri 15",
            relief=tk.FLAT, yscrollcommand=self.scrollBar.set)
        self.scrollBar.config(command=self.textArea.yview)
        self.scrollBar.pack(side=tk.RIGHT, fill=tk.Y)
        self.textArea.bind("<Key>", self.parent.keyDown)
        self.textArea.tag_config("link", foreground=os.getenv("LINK_COLOR"))
        self.textArea.tag_bind("link", "<Button-1>", self.link_callback)

        # Frequency
        journal_frame = tk.Frame(self)
        journal_text = tk.Label(journal_frame, text='Journal:')
        journal_text.pack(side=tk.LEFT)
        self.journal = tk.StringVar()
        self.journal.set(None)
        journal_entry = tk.Label(journal_frame, textvariable=self.journal, cursor="hand2")
        journal_entry.bind("<Button-1>", lambda e: self.parent.parent_action(action="open", note_title=self.journal.get()) if self.journal.get() else None)
        journal_entry.pack(side=tk.RIGHT)
        journal_frame.pack(side=tk.BOTTOM)

        # Other Note metadata
        self.dates_var = tk.StringVar()
        dateText = tk.Label(self, textvariable=self.dates_var)
        dateText.pack(side=tk.BOTTOM)

        self.textArea.pack(side=tk.LEFT, fill=tk.BOTH)

    def get_note_dict(self):
        content = self.textArea.get('1.0', tk.END)[:-1]
        # textArea is adding a newline at the end which we don't want

        return {"content": content, "journal": self.journal.get()}


class JournalPage(Note):

    name = "journalPage"
    gui_class = JournalPageContent

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if "journal" not in kwargs:
            self.metadata["journal"] = None

    def get_journal(self) -> str | None:
        return self.metadata["journal"]

    def set_journal(self, journal_title: str):
        self.metadata["journal"] = journal_title


class JournalContent(NoteContent):

    def load(self, note):
        assert type(note) is Journal

        super().load(note)

        # Set additional fields
        self.freq_option.set(Frequency(note.get_frequency()).value)

    def display(self):
        # Editor (main area)
        self.scrollBar = ttk.Scrollbar(self)
        self.textArea = tk.Text(self, font="Calibri 15",
            relief=tk.FLAT, yscrollcommand=self.scrollBar.set)
        self.scrollBar.config(command=self.textArea.yview)
        self.scrollBar.pack(side=tk.RIGHT, fill=tk.Y)
        self.textArea.bind("<Key>", self.parent.keyDown)
        self.textArea.tag_config("link", foreground=os.getenv("LINK_COLOR"))
        self.textArea.tag_bind("link", "<Button-1>", self.link_callback)

        # Frequency
        freq_frame = tk.Frame(self)
        freq_text = tk.Label(freq_frame, text='Frequency:')
        freq_text.pack(side=tk.LEFT)
        self.freq_option = tk.StringVar()
        self.freq_option.set(DEFAULT_FREQUENCY.value)
        freq_options = [freq.value for freq in Frequency]
        self.freq_menu = tk.OptionMenu(freq_frame, self.freq_option, *freq_options)
        self.freq_menu.pack(side=tk.RIGHT)
        freq_frame.pack(side=tk.BOTTOM)

        # Button to open current page
        action_frame = tk.Frame(self)
        openButton = tk.Button(action_frame, text='Open current page')
        openButton.pack(side=tk.LEFT)
        openButton.config(command=self.open_current_page)
        pagesButton = tk.Button(action_frame, text='Show all pages')
        pagesButton.pack(side=tk.RIGHT)
        pagesButton.config(command=self.show_pages)
        action_frame.pack(side=tk.BOTTOM)

        # Other Note metadata
        self.dates_var = tk.StringVar()
        dateText = tk.Label(self, textvariable=self.dates_var)
        dateText.pack(side=tk.BOTTOM)

        self.textArea.pack(side=tk.LEFT, fill=tk.BOTH)

    def show_pages(self):
        #journal_reference = self._make_ref(self.note["title"])
        #query = f"{journal_reference} --type:{JournalPage.name}"

        self.parent.do(action="search", note_type=JournalPage.name, metadata={'journal': self.note['title']})

    def open_current_page(self):
        # Get the title of the current page
        note_title = self.note["title"] + " - " + get_current_time_string(self.note.get_frequency())

        # Check if the page exists
        note = self.parent.mb[note_title]
        if note is None:
            journal_reference = self._make_ref(self.note["title"])

            note_dict = {
                "content": note_title,
                "note_type": JournalPage.name,
                "journal": self.note["title"],
            }
            self.parent.mb.add_note(create_note(note_dict))

        # Open the page
        # (In the parent of the parent to avoid opening the same note in several tabs)
        self.parent.parent_action(action="open", note_title=note_title)

    def get_note_dict(self):
        content = self.textArea.get('1.0', tk.END)[:-1]
        # textArea is adding a newline at the end which we don't want

        frequency = self.freq_option.get()

        return {"content": content, "frequency": frequency}

#TODO To add later
# - Start and end dates - perhaps not for first version though
# - Frequency number (e.g. for every other day)

class Journal(Note):

    name = "journal"
    gui_class = JournalContent

    def get_frequency(self) -> Frequency:
        if "frequency" not in self.metadata or not self.metadata["frequency"]:
            return DEFAULT_FREQUENCY
        else:
            return Frequency(self.metadata["frequency"])

    def set_frequency(self, frequency: str):
        self.metadata["frequency"] = Frequency(frequency).name


# Set special content classes
NOTE_TYPE_REGISTRY[Journal.name] = Journal
NOTE_TYPE_REGISTRY[JournalPage.name] = JournalPage
