import pytest
import datetime

from dotenv import load_dotenv

load_dotenv()

from muscularbrain.journal import (
    Frequency,
    Journal,
    JournalContent,
    get_current_time_string,
)


def test_get_current_time_string():
    present = datetime.datetime(year=2024, month=5, day=1)

    # Daily
    time_string = get_current_time_string(Frequency('daily'), time=present)
    future = present + datetime.timedelta(days=1)
    assert time_string!=get_current_time_string(Frequency('daily'), time=future)

    # Weekly
    time_string = get_current_time_string(Frequency('weekly'), time=present)
    future = present + datetime.timedelta(days=7)
    assert time_string!=get_current_time_string(Frequency('weekly'), time=future)

    # Monthly
    time_string = get_current_time_string(Frequency('monthly'), time=present)
    future = present + datetime.timedelta(days=27)
    assert time_string==get_current_time_string(Frequency('monthly'), time=future)
    future2 = present + datetime.timedelta(days=31)
    assert time_string!=get_current_time_string(Frequency('monthly'), time=future2)

    # Quarterly
    time_string = get_current_time_string(Frequency('quarterly'), time=present)
    future = present + datetime.timedelta(days=40)
    assert time_string==get_current_time_string(Frequency('quarterly'), time=future)
    future2 = present + datetime.timedelta(days=70)
    assert time_string!=get_current_time_string(Frequency('quarterly'), time=future2)

    # Yearly
    time_string = get_current_time_string(Frequency('yearly'), time=present)
    future = present + datetime.timedelta(days=180)
    assert time_string==get_current_time_string(Frequency('yearly'), time=future)
    future2 = present + datetime.timedelta(days=370)
    assert time_string!=get_current_time_string(Frequency('yearly'), time=future2)
