import re
import os
import pytest
import shutil
import datetime

from muscularbrain.system import (
    MB,
    Note,
    Link,
    REFERENCE_STRING,
    REFERENCE_REGEX,
    DEFAULT_WORKSPACE,
    DEFAULT_NOTE_TYPE,
    DELETED_NOTE_TITLE,
    Workspace,
    IntegrityError,
    NOTE_TYPE_REGISTRY,
)


NOTES = [
    "First note\n\nFirst line in first note.\nSecond line with link to " + REFERENCE_STRING % "Second note",
    "Second note\n\nFirst line in second note with link to " + REFERENCE_STRING % "Third note",
    "Third note",
    "4th-note",
]

TEST_DATA_DIR = "tests/test_data/"
MB_DIR = TEST_DATA_DIR + "MB"
KEY_FILE = TEST_DATA_DIR + "KEY"
MB_DIR2 = TEST_DATA_DIR + "MB2"

@pytest.fixture
def mb():
    # We want a directory for storing test data as well
    if not os.path.isdir(TEST_DATA_DIR):
        print("Creating test data directory")
        os.makedirs(TEST_DATA_DIR)

    if os.path.isdir(MB_DIR):
        print("Removing previous organizer files")
        shutil.rmtree(MB_DIR)
    if os.path.isfile(KEY_FILE):
        print("Removing previous key")
        os.remove(KEY_FILE)

    mb = MB(path=MB_DIR, key_path=KEY_FILE)
    print("Adding notes ...")
    for i, content in enumerate(NOTES):
        # Also set creation times, so that integrity tests are realistic
        creation_time = datetime.datetime.now() + datetime.timedelta(seconds=i)
        mb.add_note(Note(content, creation=creation_time))
    yield mb

    if os.path.isdir(MB_DIR):
        print("Removing organizer files")
        shutil.rmtree(MB_DIR)

def test_links():
    mb = MB(path=MB_DIR, key_path=KEY_FILE)

    titles = [str(i) for i in range(50)]

    text = ""
    for t in titles:
        text += REFERENCE_STRING % t

    content = "TITLE\n" + text

    links = re.findall(REFERENCE_REGEX, content)
    for link in links:
        assert link[-1] in titles
    assert len(titles)==len(links)

    mb.add_note(Note(NOTES[0]))
    assert Link("First note", "Second note") in mb.links

    if os.path.isdir(MB_DIR):
        print("Removing organizer files")
        shutil.rmtree(MB_DIR)


def test_saving(mb):
    # Note that mb already saves its contents to disk when
    # adding the notes

    # Load a new instance based on the save file
    mb2 = MB(path=MB_DIR, key_path=KEY_FILE)

    # Make sure the new instance is identical to the old one
    assert len(mb2.notes)==len(mb.notes)
    assert len(mb2.links)==len(mb.links)
    assert set(mb2.notes.keys())==set(mb.notes.keys())
    for note_title in mb.notes:
        note, note2 = mb.notes[note_title], mb2.notes[note_title]
        for key in note.keys():
            assert note2[key]==note[key]
    for link2, link in zip(mb2.links, mb.links):
        assert link2==link


def test_encryption(mb):
    # Attempt to load the state with an invalid key
    with pytest.raises(UnicodeDecodeError) as e_info:
        mb2 = MB(path=MB_DIR, key_path=KEY_FILE + "_wrong")


def test_deletion(mb):
    # Remove a Note in the middle
    title = "Third note"
    mb.delete_note(title)

    assert mb[title] is None

    # Now check if everything is still consistent
    for note_title, note in mb.notes.items():
        assert note==mb.notes[note_title]
        assert note==mb[note_title]

    # Remove a Note that is linked to
    title = "Second note"
    assert len(mb.search_links(note_B=title))>0
    mb.delete_note(title)

    assert mb[title] is None
    # The link should be removed from MB
    assert len(mb.search_links(note_B=title))==0
    # The link in the other Note should be updated now
    assert (REFERENCE_STRING % DELETED_NOTE_TITLE) in mb["First note"]["content"]


def test_integrity(mb):
    mb.note_files["Second note"] = mb.note_files["First note"]
    with pytest.raises(IntegrityError) as e_info:
        mb.edit_note("Second note", "Content here doesn't matter")

    third_file = os.path.join(mb.paths["dir"], mb.note_files["Third note"])
    fourth_file = os.path.join(mb.paths["dir"], mb.note_files["4th-note"])
    # Overwrite the fourth note with the third one
    os.rename(third_file, fourth_file)

    # Now editing the fourth note should raise an IntegrityError,
    # since the corresponding file doesn't exist anymore
    with pytest.raises(IntegrityError) as e_info:
        mb.edit_note("4th-note", "Irrelevant")

    # Make sure that our attempt to delete didn't mess things up
    assert "4th-note" in mb.notes
    assert mb["4th-note"]["title"]=="4th-note"

    # We should also get an exception when trying to delete
    # the note of which the file is missing
    with pytest.raises(IntegrityError) as e_info:
        mb.delete_note("4th-note")

def test_changing_title(mb):
    print("Initial links:", mb.links)

    num_notes = len(mb.notes)

    # Rename a Note that is linked to
    old_title = "Second note"
    note = mb[old_title]
    new_title = "EDITED " + old_title
    # Note that we also remove some content including a link to note 0
    mb.edit_note(original_title=old_title, content=new_title)
    assert mb[new_title]["title"]==new_title
    assert new_title in mb.note_files

    # Old title should not be available anymore
    assert mb[old_title] is None
    assert old_title not in mb.note_files

    # Now also check if link in first note was adjusted
    print("Links after editing:", mb.links)
    # Make sure links in the contents were adjusted
    other_title = "First note"
    assert (REFERENCE_STRING % old_title) not in mb[other_title]['content']
    assert (REFERENCE_STRING % new_title) in mb[other_title]['content']
    # Make sure the links list was also adjusted
    assert len(mb.get_links(old_title))==0
    assert len(mb.search_links(note_B=other_title))==0
    assert len(mb.get_links(new_title))==1

    # Do another small edit, which doesn't change the title
    # (Also to see if integrity of the disk file can be verified after
    #  initial renaming)
    r = mb.edit_note(new_title, content=new_title + "\n\nTest")
    assert r["title"]==new_title

    # Ensure renaming to existing note doesn't overwrite
    existing_title = "4th-note"
    with pytest.raises(ValueError) as e_info:
        # Attempt to change the title of the same Note to the title
        # of another Note
        mb.edit_note(original_title=new_title, content=existing_title)
    # After the renaming fails we should be able to fix it easily
    # Title should still be the same
    assert mb[new_title]["title"]==new_title
    # The other note should still be the same and be indexed correctly
    assert mb[existing_title]["title"]==existing_title

    # Make sure the number of notes hasn't change since the beginning
    assert len(mb.notes)==num_notes


def test_workspace(mb):
    print("Adding initial bookmarks")
    initial_bookmarks = ["Second note", "Third note"]
    for note_title in initial_bookmarks:
        mb.bookmark(note_title)
    assert mb.get_bookmarks()==initial_bookmarks

    print("Adding initial tabs")
    initial_tabs = ["Second note", "4th-note"]
    mb.set_tabs(initial_tabs)
    assert mb.get_tabs()==initial_tabs

    print("Creating a new workspace")
    new_workspace = "new workspace"
    mb.add_note(Workspace(content=new_workspace, note_type=Workspace.name))
    assert mb.get_workspaces(order_by="lastchange")[::-1]==[DEFAULT_WORKSPACE, new_workspace]
    # Make sure nothing happened before the new workspace was selected
    assert mb.active_workspace==DEFAULT_WORKSPACE
    assert mb.get_bookmarks()==initial_bookmarks
    assert mb.get_tabs()==initial_tabs

    # Then select the new workspace and make sure that bookmarks and tabs are empty
    mb.select_workspace(new_workspace)
    assert mb.get_bookmarks()==[]
    assert mb.get_tabs()==[]

    # Add bookmarks and tabs to the new workspace
    new_bookmarks = ["First note", "Third note", "4th-note"]
    for note_title in new_bookmarks:
        mb.bookmark(note_title)
    assert mb.get_bookmarks()==new_bookmarks

    new_tabs = ["First note"]
    mb.set_tabs(new_tabs)
    assert mb.get_tabs()==new_tabs

    # Remove a bookmark from the middle of the list
    mb.bookmark(new_bookmarks[1])
    assert new_bookmarks[1] not in mb.get_bookmarks()
    assert len(mb.get_bookmarks())==len(new_bookmarks)-1
    # Calling bookmark again should add it again
    mb.bookmark(new_bookmarks[1])
    assert new_bookmarks[1] in mb.get_bookmarks()
    assert len(mb.get_bookmarks())==len(new_bookmarks)

    # Switch back to old one and ensure nothing changed
    mb.bookmark(new_bookmarks[1])
    mb.select_workspace(DEFAULT_WORKSPACE)
    assert mb.get_bookmarks()==initial_bookmarks
    assert mb.get_tabs()==initial_tabs

    # Edit a bookmarked note
    mb.edit_note("Third note", "Edited third note")
    new_bookmarks = ["Second note", "Edited third note"]
    assert "Edited third note" in mb.get_bookmarks()
    assert "Third note" not in mb.get_bookmarks()

    # Edit a note that's part of a tab
    mb.edit_note("4th-note", "Edited 4th")
    assert "Edited 4th" in mb.get_tabs()
    assert "4th-note" not in mb.get_tabs()


def test_inline_creation():
    if os.path.isdir(MB_DIR2):
        print("Removing previous organizer files")
        shutil.rmtree(MB_DIR2)

    mb = MB(MB_DIR2, key_path=KEY_FILE)

    note_content = "Creation note\n\nnote{{Created note}} and workspace{{Workspace note}}"
    mb.add_note(Note(note_content))

    assert "Creation note" in mb.notes
    # Make sure both notes have been created
    assert "Created note" in mb.notes
    assert "Workspace note" in mb.notes

    # Ensure that contents of all the notes are as expected
    assert mb["Created note"]["content"] == "Created note"
    assert mb["Workspace note"]["content"] == "Workspace note"
    assert mb["Creation note"]["content"] == "Creation note\n\n" + REFERENCE_STRING % "Created note" + " and " + REFERENCE_STRING % "Workspace note"

    assert mb["Created note"].metadata["type"] == "note"
    assert mb["Workspace note"].metadata["type"] == "workspace"
    assert "Workspace note" in mb.get_workspaces()

    # Verify that links were created
    assert len(mb.search_links(note_A="Creation note"))==2
    assert len(mb.search_links(note_B="Created note"))==1
    assert len(mb.search_links(note_B="Workspace note"))==1

    # Check that no additional links were created
    assert len(mb.search_links(note_B="Creation note"))==0
    assert len(mb.search_links(note_A="Created note"))==0
    assert len(mb.search_links(note_A="Workspace note"))==0

    # Cleanup
    shutil.rmtree(MB_DIR2)


def test_type_changes(mb):
    import muscularbrain.journal  # This activates special displaying of journal contents
    from muscularbrain.journal import Journal

    assert "journal" in NOTE_TYPE_REGISTRY

    title = "First note"
    note = mb[title]
    original_dict = note.to_dict()
    assert note.metadata["type"]==DEFAULT_NOTE_TYPE
    mb.edit_note(note["title"], content=note["content"], note_type="journal")
    note = mb[title]
    assert note.metadata["type"]=="journal"
    assert type(note) is Journal

    mb.edit_note(note["title"], content=note["content"], note_type=DEFAULT_NOTE_TYPE)
    note = mb[title]
    assert note.metadata["type"]==DEFAULT_NOTE_TYPE
    assert type(note) is Note

    note_dict = note.to_dict()
    for key in note_dict:
        assert note_dict[key]==original_dict[key]
    for key in original_dict:
        assert key in note_dict


def test_multiple_renaming(mb):
    # The following problematic bug happend before and should be avoided:
    # - Situation: Two notes, second note having link to first note
    # - Then change title of first note.
    # - Afterwards, trying to change title of second note, we get an exception related to links

    original_first = "Second note"
    original_second = "First note"  # Has a reference to original_first

    print("Links before changing anything:", mb.links)
    first_title = "New title for first note"
    mb.edit_note(original_first, content=first_title)
    print("Links after changing first note:", mb.links)
    second_content = mb[original_second]['content']  # This is after renaming the first note, so the reference should be adjusted!
    second_title = "New title for second note"
    mb.edit_note(original_second, content=f"{second_title}\n\n{second_content.split('\n', 1)[1]}")
    print("Links after changing second note:", mb.links)

    link = Link(in_note=second_title, out_note=first_title)
    assert link in mb.links
