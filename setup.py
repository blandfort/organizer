import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

requirements = [r for r in open("requirements.txt", "r")]

setuptools.setup(
    name="MuscularBrain",
    version="2.1",
    author="Philipp Blandfort",
    description="Python-based personal organizer for taking notes and storing them in encrypted format",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/blandfort/organizer",
    packages=setuptools.find_packages(include=['muscularbrain', 'muscularbrain.*']),
    install_requires=requirements,
    extras_require={
        "dev": [
            "pytest",
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # Python3.11 causes issues on MacOS!
    python_requires='>=3.7',
)
