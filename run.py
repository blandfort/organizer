import os
from dotenv import load_dotenv

load_dotenv()

# Note that we want to load MB _after_ setting environment variables
from muscularbrain import MB, MBUI

import muscularbrain.journal  # This activates special displaying of journal contents


if __name__=='__main__':
    mb = MB(
        path=os.getenv("MB_PATH"),
        key_path=os.getenv("KEY_PATH"),
        enable_log=False,
    )
    ui = MBUI(mb=mb)
